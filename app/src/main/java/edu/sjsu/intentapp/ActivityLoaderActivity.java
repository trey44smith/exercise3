package edu.sjsu.intentapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityLoaderActivity extends AppCompatActivity {

    Button button;
    Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);

        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view){
                String myWebsiteUri = "http://www.amazon.com";

                Intent intent2 = new Intent(Intent.ACTION_VIEW);
                Intent chooser = Intent.createChooser(intent2, "Choose your browser");
                startActivity(chooser);
            }
        });

        button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view){
                String myPhoneNumberUri = "tel:+194912344444";

                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(myPhoneNumberUri));
                startActivity(intent);
            }
        });
    }
}
